#include "pessoa.hpp"
#include <iostream>

using namespace std;

Pessoa::Pessoa(){
    cout << "Construtor da classe Pessoa" << endl;
    set_nome("");
    set_cpf(0);
    set_telefone("");
    set_email(""); 
    cout << "";   
}
Pessoa::~Pessoa(){
    cout << "Destrutor da classe Pessoa" << endl;
}

string Pessoa::get_nome(){
    return nome;
}
void Pessoa::set_nome(string nome){
    this->nome = nome;
}
long int Pessoa::get_cpf(){
    return cpf;
}
void Pessoa::set_cpf(long int cpf){
    this->cpf = cpf;
}
string Pessoa::get_telefone(){
    return telefone;
}
void Pessoa::set_telefone(string telefone){
    this->telefone = telefone;
}
string Pessoa::get_email(){
    return email;
}   
void Pessoa::set_email(string email){
    this->email = email;
}














